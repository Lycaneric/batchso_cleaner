"""
batchso_cleaner by Eric Wideman
3-21-2017
"""

"""
Run this query first to get the list of files.

SELECT ExternalUID
FROM SalesHeader_Interface
LEFT OUTER JOIN DC00TAG..OrderHeader OrderHeader
	ON OrderHeader.ETFNo = SalesHeader_Interface.ETFNo
where SalesHeader_Interface.UpdatePID <> 'DocCorrect'
	AND SalesHeader_Interface.SoldToAccountNbr = 'IGNRENO'
	AND SalesHeader_Interface.OrderSource = 'PRTL'
	AND SalesHeader_Interface.InterfaceStatus LIKE 'Error%'
	AND SalesHeader_Interface.ExternalUID LIKE 'NWL%'
	AND SalesHeader_Interface.InterfaceDate > dateadd(day, -30, getdate())
	AND OrderHeader.OrderID IS NULL
"""

import csv
import glob
import os
import pymssql

from os import getenv

#connect to the database
server = ("stl01sql2008")
user = ("felix")
password = ("Foolish rank groggy Iaccoca")

conn = pymssql.connect(server, user, password, "CDITAG")
cursor = conn.cursor(as_dict=True)

#execute a SQL statement
cursor.execute("""
SELECT ExternalUID
FROM SalesHeader_Interface
LEFT OUTER JOIN DC00TAG..OrderHeader OrderHeader
	ON OrderHeader.ETFNo = SalesHeader_Interface.ETFNo
where SalesHeader_Interface.UpdatePID <> 'DocCorrect'
	AND SalesHeader_Interface.SoldToAccountNbr = 'IGNRENO'
	AND SalesHeader_Interface.OrderSource = 'PRTL'
	AND SalesHeader_Interface.InterfaceStatus LIKE 'Error%'
	AND SalesHeader_Interface.ExternalUID LIKE 'NWL%'
	AND SalesHeader_Interface.InterfaceDate > dateadd(day, -30, getdate())
	AND OrderHeader.OrderID IS NULL
""")

rows = cursor.fetchall()

error_list = []

for row in rows:
	r = row['ExternalUID']
	error_list.append(r)

error_list = set(error_list)

#directory where the files are located, a mounted drive in this instance
directory = '/home/eric/batch_so'

#go to that directory and make a list of all the files that match the pattern
os.chdir(directory)

all_files = glob.glob('PRTL.NWL*')

#delete the files that do not belong in the list
for filename in all_files:
	f = filename.split('.')
	f = f[1]
	if f not in error_list:
			try:
				os.remove(filename)
				print('File ' + f + ' deleted.')
			except ValueError:
				print('Could not delete file ' + f + '.')
